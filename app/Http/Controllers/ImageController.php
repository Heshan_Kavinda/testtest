<?php

namespace App\Http\Controllers;
use App\image;
use App\Slider;
use Illuminate\Http\Request;

class ImageController extends Controller
{

    public function showuploadForm(){
       return view('Upload');
    }

    public function StoresFile(request $request){

        $this->validate($request,[
          'Category'=>'required',
          'ExpireDate'=>'required|date|after:today',
          'image'=>'required',
          'TransitionTime'=>'required|numeric|min:30|max:120'
        ]);

        if($request->hasFile('image')){
            $TT = $request->TransitionTime;
            $ED = $request->ExpireDate;
            $C = $request->Category;

          foreach ($request->image as $file){
            $filename = $file->getClientOriginalName();
            $file->move(public_path(). '/',$filename);
            $sliders = new Slider;
            $sliders->image_path = $filename;
            $sliders->Transition_time = $TT;
            $sliders->expired_date = $ED;
            $sliders->state = "Pending" ;
            $sliders->category = $C;
            $sliders->save();
          }
        }
          return redirect('/Upload')->with('success','Images Uploaded');
}
}
